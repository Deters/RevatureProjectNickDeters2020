import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import DAOPack.ReimbDAO;
import DAOPack.ReimbDAOimpl;
import Models.ReUserModel;
import Models.UserView;
import TestService.ServiceImp;
import TestService.ServiceLayer;

public class Project1TestCases {
	
	ServiceLayer ServiceTest = new ServiceImp();
	ReimbDAO DAOtest = new ReimbDAOimpl();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	//READ
	@Test
	public void test() {
		List<UserView> getList = DAOtest.getViewList();
		int listSize = getList.size();
		
		
		assertEquals("testing if my list size is correct",19, listSize);
	}
	
	@Test
	public void test1() {
		List<UserView> getListTest = DAOtest.getPendingList();
		int listSize = getListTest.size();
		assertEquals("testing to see if ", 1, listSize);
	}
	
	@Test
	public void test2() {
		String loginName = "Matrius";
		ReUserModel test = new ReUserModel();
		
		DAOtest.getGamerobj(test, loginName);
		
		assertEquals("Testing if the Matrius object is found and set correctly", "Matrinen", test.getFirstName() );
		assertEquals("Testing if object is good", "Alkrack", test.getLastName());
		
	}
	
	
	@Test
	public void test3() {
		
		
		
		assertEquals("Testing if the user is found", true, ServiceTest.ServiceImp("Franz", "Hammer"));
		assertEquals("Testing if the user is found", false, ServiceTest.ServiceImp("ZZZ", "Trains"));
		
		
	}
	
	@Test
	public void test4() {
		List<UserView> getListTest = ServiceTest.getAdminView();
		int containTest = getListTest.size();
		
		assertEquals("Testing if the size of my full adminview = ", 20, containTest);
		assertNotEquals("Testing if the size of my full adminview = ", 12, containTest);
		
	}
	
	@Test
	public void test5() {
		
		assertEquals("Testing if the Scribe is not found", false, ServiceTest.CheckAdmin("Matranin", "Admin"));
		assertEquals("Testing if the Scribe is not found", true, ServiceTest.CheckAdmin("Matrius", "Admin"));
		
		
	}

}
