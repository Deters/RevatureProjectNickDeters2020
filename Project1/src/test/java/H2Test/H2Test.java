package H2Test;

import static org.junit.Assert.*;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import DAOPack.ReimbDAO;
import DAOPack.ReimbDAOimpl;
import MyConnectionFactory.MyConnection;

public class H2Test {
	
	 private static ReimbDAO DAOtest;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		MyConnection.url = MyConnection.h2url;
		MyConnection.username = MyConnection.h2Username;
		MyConnection.password = MyConnection.h2Passowrd;
		
		DAOtest = new ReimbDAOimpl();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		DAOtest.initializeH2DAO();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		DAOtest.geth2Test();
	}
	
	
	@Test
	public void test1() {
		DAOtest.geth2Test();
	}

}
