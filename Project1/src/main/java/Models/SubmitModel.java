package Models;

public class SubmitModel {
	private int reImbId;
	private float reImbAmount;
	private String descip;
	private int authReImb;
	private int resoReimb;
	private int reImbStat;
	private int reImbType;
	
	public SubmitModel() {
		
	}

	

	public int getReImbId() {
		return reImbId;
	}

	public void setReImbId(int reImbId) {
		this.reImbId = reImbId;
	}

	

	public SubmitModel(int reImbId, float reImbAmount, String descip, int authReImb, int resoReimb, int reImbStat,
			int reImbType) {
		super();
		this.reImbId = reImbId;
		this.reImbAmount = reImbAmount;
		this.descip = descip;
		this.authReImb = authReImb;
		this.resoReimb = resoReimb;
		this.reImbStat = reImbStat;
		this.reImbType = reImbType;
	}



	public float getReImbAmount() {
		return reImbAmount;
	}



	public void setReImbAmount(float reImbAmount) {
		this.reImbAmount = reImbAmount;
	}



	public String getDescip() {
		return descip;
	}

	public void setDescip(String descip) {
		this.descip = descip;
	}

	public int getAuthReImb() {
		return authReImb;
	}

	public void setAuthReImb(int authReImb) {
		this.authReImb = authReImb;
	}

	public int getResoReimb() {
		return resoReimb;
	}

	public void setResoReimb(int resoReimb) {
		this.resoReimb = resoReimb;
	}

	public int getReImbStat() {
		return reImbStat;
	}

	public void setReImbStat(int reImbStat) {
		this.reImbStat = reImbStat;
	}

	public int getReImbType() {
		return reImbType;
	}

	public void setReImbType(int reImbType) {
		this.reImbType = reImbType;
	}

	@Override
	public String toString() {
		return "SubmitModel [reImbId=" + reImbId + ", reImbAmount=" + reImbAmount + ", descip=" + descip
				+ ", authReImb=" + authReImb + ", resoReimb=" + resoReimb + ", reImbStat=" + reImbStat + ", reImbType="
				+ reImbType + "]";
	}

	

}
