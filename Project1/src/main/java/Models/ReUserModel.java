package Models;

public class ReUserModel {
	
	private int userId;
	private String useName;
	private String usePass;
	private String firstName;
	private String lastName;
	private String email;
	private int userRole;
	
	
	public ReUserModel() {
		
	}


	public ReUserModel(int userId, String useName, String usePass, String firstName, String lastName, String email,
			int userRole) {
		super();
		this.userId = userId;
		this.useName = useName;
		this.usePass = usePass;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.userRole = userRole;
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public String getUseName() {
		return useName;
	}


	public void setUseName(String useName) {
		this.useName = useName;
	}


	public String getUsePass() {
		return usePass;
	}


	public void setUsePass(String usePass) {
		this.usePass = usePass;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public int getUserRole() {
		return userRole;
	}


	public void setUserRole(int userRole) {
		this.userRole = userRole;
	}


	@Override
	public String toString() {
		return "ReUserModel [userId=" + userId + ", useName=" + useName + ", usePass=" + usePass + ", firstName="
				+ firstName + ", lastName=" + lastName + ", email=" + email + ", userRole=" + userRole + "]";
	}
	
	
	
	

}
