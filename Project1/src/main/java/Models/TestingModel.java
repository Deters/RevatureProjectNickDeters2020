package Models;




public class TestingModel {

	private int TestMod;
	private String TestString;
	
	
	public TestingModel(){
		
	}


	public TestingModel(int testMod, String testString) {
		super();
		TestMod = testMod;
		TestString = testString;
	}


	@Override
	public String toString() {
		return "TestingModel [TestMod=" + TestMod + ", TestString=" + TestString + "]";
	}


	public int getTestMod() {
		return TestMod;
	}


	public void setTestMod(int testMod) {
		TestMod = testMod;
	}


	public String getTestString() {
		return TestString;
	}


	public void setTestString(String testString) {
		TestString = testString;
	}

	

	
}
