package Models;

public class ReImbModel {
	
	private int reImbId;
	private float reImbAmount;
	private long timeStampSub;
	private long timeStampResolve;
	private String descip;
	private int authReImb;
	private int resoReimb;
	private int reImbStat;
	private int reImbType;
	
	
	public ReImbModel() {
		
	}


	


	@Override
	public String toString() {
		return "ReImbModel [reImbId=" + reImbId + ", reImbAmount=" + reImbAmount + ", timeStampSub=" + timeStampSub
				+ ", timeStampResolve=" + timeStampResolve + ", descip=" + descip + ", authReImb=" + authReImb
				+ ", resoReimb=" + resoReimb + ", reImbStat=" + reImbStat + ", reImbType=" + reImbType + "]";
	}





	public ReImbModel(int reImbId, float reImbAmount, long timeStampSub, long timeStampResolve, String descip,
			int authReImb, int resoReimb, int reImbStat, int reImbType) {
		super();
		this.reImbId = reImbId;
		this.reImbAmount = reImbAmount;
		this.timeStampSub = timeStampSub;
		this.timeStampResolve = timeStampResolve;
		this.descip = descip;
		this.authReImb = authReImb;
		this.resoReimb = resoReimb;
		this.reImbStat = reImbStat;
		this.reImbType = reImbType;
	}





	public int getReImbId() {
		return reImbId;
	}


	public void setReImbId(int reImbId) {
		this.reImbId = reImbId;
	}


	


	

	public float getReImbAmount() {
		return reImbAmount;
	}


	public void setReImbAmount(float reImbAmount) {
		this.reImbAmount = reImbAmount;
	}


	public long getTimeStampSub() {
		return timeStampSub;
	}


	public void setTimeStampSub(long timeStampSub) {
		this.timeStampSub = timeStampSub;
	}


	public long getTimeStampResolve() {
		return timeStampResolve;
	}


	public void setTimeStampResolve(long timeStampResolve) {
		this.timeStampResolve = timeStampResolve;
	}


	public String getDescip() {
		return descip;
	}


	public void setDescip(String descip) {
		this.descip = descip;
	}


	public int getAuthReImb() {
		return authReImb;
	}


	public void setAuthReImb(int authReImb) {
		this.authReImb = authReImb;
	}


	public int getResoReimb() {
		return resoReimb;
	}


	public void setResoReimb(int resoReimb) {
		this.resoReimb = resoReimb;
	}


	public int getReImbStat() {
		return reImbStat;
	}


	public void setReImbStat(int reImbStat) {
		this.reImbStat = reImbStat;
	}


	public int getReImbType() {
		return reImbType;
	}


	public void setReImbType(int reImbType) {
		this.reImbType = reImbType;
	}


	
	
	
	

}
