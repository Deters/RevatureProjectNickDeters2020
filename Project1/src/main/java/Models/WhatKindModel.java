package Models;

public class WhatKindModel {
	
	private String useName;
	private String passWord;
	private String roleOf;
	
	
	public WhatKindModel() {
		
	}


	public WhatKindModel(String useName, String passWord, String roleOf) {
		super();
		this.useName = useName;
		this.passWord = passWord;
		this.roleOf = roleOf;
	}


	public String getUseName() {
		return useName;
	}


	public void setUseName(String useName) {
		this.useName = useName;
	}


	public String getPassWord() {
		return passWord;
	}


	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}


	public String getRoleOf() {
		return roleOf;
	}


	public void setRoleOf(String roleOf) {
		this.roleOf = roleOf;
	}


	@Override
	public String toString() {
		return "WhatKindModel [useName=" + useName + ", passWord=" + passWord + ", roleOf=" + roleOf + "]";
	}
	
	
	

}
