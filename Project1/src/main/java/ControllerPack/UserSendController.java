package ControllerPack;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;

import Models.ReImbModel;
import Models.ReUserModel;
import TestService.ServiceImp;
import TestService.ServiceLayer;

public class UserSendController {
	
	
	
	public static void SendJson(HttpServletRequest req, HttpServletResponse res)  throws JsonProcessingException, IOException {
		
		res.setContentType("application/json");
		//int reImbId = Integer.parseInt(req.getParameter("reImb"));
		//Serialize the ID in the database
		
		float reImbAmount = Float.parseFloat(req.getParameter("amount"));
		String descip = req.getParameter("description");
//		int authReImb = Integer.parseInt(req.getParameter("reImbAuther"));
		// get there name ID from the DB then put it into the session after just use the 
		// res.getsession() attribute
				
		int reImbType = Integer.parseInt(req.getParameter("type"));
		
		System.out.println(reImbType);
		System.out.println(descip);
		System.out.println(reImbAmount);
		
		ReUserModel userLogged = (ReUserModel)req.getSession().getAttribute("UserObject");
		
		ReImbModel Request = new ReImbModel();
		
		Request.setReImbAmount(reImbAmount);
		Request.setDescip(descip);
		Request.setAuthReImb(userLogged.getUserId());
		Request.setReImbStat(1);
		Request.setReImbType(reImbType);
		
		
		ServiceLayer Inserting = new ServiceImp();
		
		Inserting.insertReq(Request);
		
		
		
	}

	
	
	
}
