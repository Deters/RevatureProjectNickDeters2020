package ControllerPack;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Logi4jPack.LogUtil;
import Models.ReUserModel;
import TestService.ServiceImp;
import TestService.ServiceLayer;

public class LoginController {

	public static String login(HttpServletRequest req, HttpServletResponse res) {
		
		if(!req.getMethod().equals("POST")) {
			return "/index.html";
		}
		
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		LogUtil.logger.info(username +" Has Logged in");
		
		// System.out.println("Checkpoint 1");
		
		//check to see if the user has the correct username and password
		
		//boolean foundUserAndPassword = false;
		
		
		ServiceLayer finding = new ServiceImp();
		
		
		
		boolean Founded = finding.ServiceImp(username, password);
		
		
		System.out.println(Founded);
		
		
		
		//System.out.println(foundUserAndPassword);
		System.out.println(username);
		System.out.println(password);
		System.out.println(Founded);
		
		//the login test that's hardcoded with the example
		if(Founded == false) {
			//you'll actually go to the database to get the credentials, don't hardcode them
			return "/forwarding/incorrectcredentials";
			
		}else{
			
			
		ServiceLayer GetGamer = new ServiceImp();
			
			ReUserModel User = new ReUserModel();
			String LoginName = username;
			
			GetGamer.GetGamer(User, LoginName);
			
			System.out.println(User.getUserId() + "  WE HAVE THE ID FOR THE USER");
			
			
			
			
			
			
			req.getSession().setAttribute("UserObject" , User);
//			req.getSession().setAttribute("UserID", User.getUserId());
			req.getSession().setAttribute("loggedusername", username);
			req.getSession().setAttribute("loggedpassword", password);
			
			System.out.println(req.getSession().getAttribute("UserObject"));
			
			boolean Admin = false;
			
			ServiceLayer checkAdmin = new ServiceImp();
			
			Admin = checkAdmin.CheckAdmin(username, password);
			
			if(Admin == true) {
				//res.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
				return "/forwarding/scribeHome";
				
				
			}
			
			
			//System.out.println("Checkpoint 2");
			//you probably wil have a user object that you put into the session
			// that contains the username & password...this is just an example
//			req.getSession().setAttribute("loggedusername", username);
//			req.getSession().setAttribute("loggedpassword", password);
//			
//			ServiceLayer gamerList = new ServiceImp();
//			List<UserView> uniqueUseList = new ArrayList<>();
//			
//			uniqueUseList = gamerList.getUserView(username, password);
//			
//			req.getSession().setAttribute("User RequestList", uniqueUseList.toArray());
			//req.getSession().setAttribute("User RequestList", uniqueUseList);
			
			
			
			//ServiceLayer 
			
			//req.getSession().setAttribute("List of Requests", ListRequest);
			
			//res.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
			return "/forwarding/home";

		}
		
		

	}

	public static void logout(HttpServletRequest req, HttpServletResponse res) throws IOException {
		System.out.println("in Logout from Login Controller");
		LogUtil.logger.info(req.getSession().getAttribute("loggedusername") +" Has Logged out");
		HttpSession session = req.getSession();
		session.invalidate();
		
		res.sendRedirect("http://localhost:9001/Project1/resources/html/Index.html");
		
		
	}

}
