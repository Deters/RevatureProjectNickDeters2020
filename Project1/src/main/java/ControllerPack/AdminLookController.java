package ControllerPack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import Models.UserView;
import TestService.ServiceImp;
import TestService.ServiceLayer;

public class AdminLookController {

	public static void Display(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		
		ServiceLayer gamerList = new ServiceImp();
		List<UserView> userList = new ArrayList<>();
		
		
		userList = gamerList.getAdminView();

		System.out.println(userList);
		
		
		res.getWriter().write(new ObjectMapper().writeValueAsString(userList));
		
		
	}

}
