package MyConnectionFactory;

import Logi4jPack.LogUtil;

public class MyConnection {
	
	static { 
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Static block has failed me");
            LogUtil.logger.warn("Exception creating a new customer", e);
        }
    }
	
	
	public static String url = "jdbc:postgresql://database-1.cogmp58qv02e.us-east-2.rds.amazonaws.com/ERS";
	public static String username = System.getenv("DBUserName");
	public static String password = System.getenv("DBPassword");

	
	public static String h2url = "jdbc:h2:./h2Data/theData";
	public static String h2Username = "sa";
	public static String h2Passowrd = "sa";
	

}
