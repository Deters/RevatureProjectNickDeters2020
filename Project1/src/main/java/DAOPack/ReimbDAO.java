package DAOPack;

import java.util.List;

import Models.ReImbModel;
import Models.ReUserModel;
import Models.SubmitModel;
import Models.UserView;
import Models.WhatKindModel;
import TestService.ServiceLayer;


public interface ReimbDAO {

		
		//CRUD METHODS ONLY 
		
		//CREATE
		public ReImbModel insertMethod(ReImbModel obj);
		
		
		//READ 
		public List<ReImbModel> selectAllTestList();
		public ReImbModel SelectModel(ReImbModel obj);
		public List<ReUserModel> seeAllList();
		
		
		
		//UPDATE LIST
		public ReImbModel updateDBMod(ReImbModel obj);
		
		
		//DELETE
		public ReImbModel delete(ReImbModel obj);


		public List<UserView> getViewList();


		public List<WhatKindModel> getWhatKindList();


		public void getGamerobj(ReUserModel user, String loginName);


		public void insertRequest(ReImbModel request);


		List<UserView> getViewReciepts();


		public void InsertStatChange(ReImbModel resolving);


		public List<UserView> getPendingList();
		
		
		public List<UserView> geth2Test();
		
		
		public void initializeH2DAO();


		


		


		
		
		

}
