package DAOPack;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import Models.TestingModel;

public class TestDAOImp implements TestDAO {
	
	public static String url = "jdbc:postgresql://database-1.cogmp58qv02e.us-east-2.rds.amazonaws.com/ERS";
	public static String username = "myDatabase";
	public static String password = "p4ssw0rd";
	
	
	

	@Override
	public TestingModel insertTest(TestingModel Mod1) {
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "Insert into Test values(?,?)";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setInt(1, Mod1.getTestMod());
			ps.setString(2, Mod1.getTestString());
			ps.execute();


			
			
			
		}catch(SQLException e ) {
			e.printStackTrace();
		}
		
		
		
		
		return null;
	}

	@Override
	public List<TestingModel> selectAllTestList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TestingModel selectByInt(int TestMod) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TestingModel selectByString(String testString) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TestingModel updateDB(TestingModel Mod2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TestingModel delete(TestingModel Mod3) {
		// TODO Auto-generated method stub
		return null;
	}

}
