package DAOPack;

import Models.TestingModel;
import java.util.List;



public interface TestDAO {

	//CRUD METHODS ONLY 
	
	//CREATE
	public TestingModel insertTest(TestingModel Mod1);
	
	//READ
	public List<TestingModel> selectAllTestList();
	public TestingModel selectByInt(int TestMod);
	public TestingModel selectByString(String testString);
	
	
	//Update List
	public TestingModel updateDB(TestingModel Mod2);
	
	
	//Delete From List
	public TestingModel delete(TestingModel Mod3);
	
	
	
	
	
	
	
	
	
	
	
	
	
}
