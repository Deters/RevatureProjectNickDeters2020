package DAOPack;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import Logi4jPack.LogUtil;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;




import Models.ReImbModel;
import Models.ReUserModel;
import Models.SubmitModel;
import Models.UserView;
import Models.WhatKindModel;
import TestService.ServiceLayer;
import MyConnectionFactory.MyConnection;

public class ReimbDAOimpl implements ReimbDAO {
	
	
	public static String url = "jdbc:postgresql://database-1.cogmp58qv02e.us-east-2.rds.amazonaws.com/ERS";
	public static String username = System.getenv("DBUserName");
	public static String password = System.getenv("DBPassword");
	
	//have to use this with WAR project in my DAO Layer
	static { 
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Static block has failed me");
            LogUtil.logger.warn("Exception creating a new customer", e);
        }
    }

	
	
	
	

	@Override
	public ReImbModel insertMethod(ReImbModel obj) {
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
		String sql = "INSERT INTO MONEYBACK values(?,?,?,?,?,?,?,?,?)";
			
		PreparedStatement ps = conn.prepareStatement(sql);
		
		ps.setInt(1,  obj.getReImbId());
		ps.setBigDecimal(2, BigDecimal.valueOf(obj.getReImbAmount()));
		ps.setLong(3, obj.getTimeStampSub());
		ps.setLong(4, obj.getTimeStampResolve());
		ps.setString(5, obj.getDescip());
		ps.setInt(6, obj.getAuthReImb());
		ps.setInt(7, obj.getResoReimb());
		ps.setInt(8, obj.getReImbStat());
		ps.setInt(9, obj.getReImbType());
		ps.execute();
		
		
		}catch(SQLException e ) {
			e.printStackTrace();
			LogUtil.logger.warn("Exception creating a new customer", e);
		}
			
			
		return null;
	}

	@Override
	public List<ReImbModel> selectAllTestList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReImbModel SelectModel(ReImbModel obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReImbModel updateDBMod(ReImbModel obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReImbModel delete(ReImbModel obj) {
		// TODO Auto-generated method stub
		return null;
	}

	
	/***
	 * @author Nicho
	 * this method is to get the list in java and see all the 
	 * in the method to see if any username and password is found
	 * returning the list
	 */
	@Override
	public List<ReUserModel> seeAllList() {
		List<ReUserModel> LookList = new ArrayList<>();
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			String sql = "SELECT gamer_usename, gamer_password FROM GAMER_USER";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				LookList.add(new ReUserModel(0, rs.getString(1), rs.getString(2), "lol", "lol", "lol", 0));
			}
			
			
			
		}catch(SQLException e) {
			e.printStackTrace();
			LogUtil.logger.warn("Exception creating a new customer", e);
		}
		
		
		return LookList;
	}

	@Override
	public List<UserView> getViewList() {
		
		List<UserView> getList = new ArrayList<>();
		
		try(Connection conn = DriverManager.getConnection(MyConnection.url, MyConnection.username, MyConnection.password)){
			
			String sql = "select * from table_data "  ;
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				getList.add(new UserView(rs.getInt(1), rs.getString(2), rs.getString(3)
						, rs.getString(4), rs.getString(5), rs.getString(6),
						rs.getString(7), rs.getString(8), rs.getString(9)));
			}
			
			
		}catch(SQLException e) {
			e.printStackTrace();
			LogUtil.logger.warn("Exception creating a new customer", e);
		}
				return getList;
	}

	@Override
	public List<WhatKindModel> getWhatKindList() {
		
		List<WhatKindModel> getList = new ArrayList<>();
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "select * from whatkind";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				getList.add(new WhatKindModel(rs.getString(1), rs.getString(2), rs.getString(3)));
			}
			
			
		}catch(SQLException e) {
			e.printStackTrace();
			LogUtil.logger.warn("Exception creating a new customer", e);
		}
		
		
		return getList;
	}

	@Override
	public void getGamerobj(ReUserModel user, String loginName) {
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT *  From gamer_user gu where gamer_usename = '" + loginName + "'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				user.setUserId(rs.getInt(1));
				user.setUseName(rs.getString(2));
				user.setUsePass(rs.getString(3));
				user.setFirstName(rs.getString(4));
				user.setLastName(rs.getString(5));
				user.setEmail(rs.getString(6));
				user.setUserRole(rs.getInt(7));
			}
			
		
		}catch(SQLException e) {
			e.printStackTrace();
			LogUtil.logger.warn("Exception creating a new customer", e);
		}
			
	}

	@Override
	public void insertRequest(ReImbModel request) {
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
		
			String sql = "INSERT INTO moneyre(\r\n" + 
					"	Moneyask  \r\n" + 
					"	,Timein  \r\n" +
					"   ,Timeresolv \r\n"+
					"	,Desciption \r\n" + 
					"	,Asking \r\n" + 
					"	,Status_of \r\n" + 
					"	,typeAsk \r\n" + 
					"	) VALUES(?,default ,Null,?,?,?,?);";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setBigDecimal(1, BigDecimal.valueOf(request.getReImbAmount()));
			ps.setString(2, request.getDescip());
			ps.setInt(3, request.getAuthReImb());
			ps.setInt(4, request.getReImbType());
			ps.setInt(5, request.getReImbStat());
			ps.execute();
			
			System.out.println("In the last part of the DAO");
			
			
			
			
		}catch(SQLException e) {
			e.printStackTrace();
			LogUtil.logger.warn("Exception creating a new customer", e);
		}
			
	}

	
	@Override
	public List<UserView> getViewReciepts() {
		
		List<UserView> UserList = new ArrayList<>();
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = " select * from table_data "  ;
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				UserList.add(new UserView(rs.getInt(1), rs.getString(2), rs.getString(3)
						, rs.getString(4), rs.getString(5), rs.getString(6),
						rs.getString(7), rs.getString(8), rs.getString(9)));
			}
			
			
		}catch(SQLException e) {
			e.printStackTrace();
			LogUtil.logger.warn("Exception creating a new customer", e);
		}
				return UserList;
	}

	@Override
	public void InsertStatChange(ReImbModel resolving) {
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "update moneyre\r\n" + 
					"set timeresolv = default , resolving = "+ resolving.getAuthReImb() + ", typeask = " + resolving.getReImbStat() +
					"where moneyback_id = " + resolving.getReImbId();
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.execute();
		
		}catch(SQLException e) {
			e.printStackTrace();
			LogUtil.logger.warn("Exception creating a new customer", e);
		}
	}

	@Override
	public List<UserView> getPendingList() {
		
		List<UserView> UserPendingList = new ArrayList<>();
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
		
			String sql = "select * from userloginstatus where status = 'Pending'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				UserPendingList.add(new UserView(rs.getInt(1), rs.getString(2), rs.getString(3)
						, rs.getString(4), rs.getString(5), rs.getString(6),
						rs.getString(7), rs.getString(8), rs.getString(9)));
			}
			
			
			
		
		}catch(SQLException e) {
			e.printStackTrace();
			LogUtil.logger.warn("Exception creating a new customer", e);
		}
			
		return UserPendingList;
	}

	
	/*** @author Nicho
	 * Just making an h2 test in everything below for learning purposes
	 */
	
	
	@Override
	public List<UserView> geth2Test() {
		
		List<UserView> h2List = new ArrayList<>();
		
		try(Connection conn = DriverManager.getConnection("jdbc:h2:./h2Data/theData", "sa", "sa")){
			
			String sql = "select * from Table_data";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				h2List.add(new UserView(rs.getInt(1), rs.getString(2), rs.getString(3)
						, rs.getString(4), rs.getString(5), rs.getString(6),
						rs.getString(7), rs.getString(8), rs.getString(9)));
			}
			
		
		}catch(SQLException e) {
			e.printStackTrace();
			LogUtil.logger.warn("Exception creating a new customer", e);
		}
		return h2List;
	}

	@Override
	public void initializeH2DAO() {
		try(Connection conn = DriverManager.getConnection("jdbc:h2:./h2Data/theData", "sa", "sa")){
			
			String sql = "CREATE TABLE Roles(\r\n" + 
					"	Role_ID int\r\n" + 
					"	,Roleof varchar(20)\r\n" + 
					"	,PRIMARY KEY(Role_ID)\r\n" + 
					");\r\n" + 
					"Create Table Gamer_User(\r\n" + 
					"	Gamer_ID int\r\n" + 
					"	,Gamer_usename varchar(50) UNIQUE\r\n" + 
					"	,Gamer_password varchar(50)\r\n" + 
					"	,Gamer_FN varchar(50)\r\n" + 
					"	,Gamer_LN varchar(50)\r\n" + 
					"	,Gamer_email varchar(500)\r\n" + 
					"	,Gamer_role int\r\n" + 
					"	,PRIMARY KEY(Gamer_ID)\r\n" + 
					"	,FOREIGN KEY (Gamer_role) References Roles (Role_ID)\r\n" + 
					");" +
					"INSERT INTO GAMER_USER VALUES(1, 'Tiber', 'ESO', 'Tiberius', 'Septum', 'Septum@mail', 2);\r\n" + 
					"INSERT INTO GAMER_USER VALUES(2, 'Franz', 'Hammer', 'Karl', 'Fanzius', 'Franz@mail', 2);\r\n" + 
					"INSERT INTO GAMER_USER VALUES(3, 'Matrius', 'Admin', 'Matrinen', 'Alkrack', 'admin@mail', 1);\r\n" + 
					"insert into gamer_user values(4, 'Kelnen', 'Admin123', 'Kelner', 'Kraisen', 'kalmer@emial.com', 1);" +
					"CREATE TABLE STATUS(\r\n" + 
					"	status_ID int\r\n" + 
					"	,status varchar(10)\r\n" + 
					"	,PRIMARY KEY(status_ID)\r\n" + 
					");" + 
					"CREATE TABLE TYPEOF(\r\n" + 
					"	Typeof_ID int\r\n" + 
					"	,typeof_Re varchar(20)\r\n" + 
					"	,PRIMARY KEY(Typeof_ID)\r\n" + 
					");" + 
					"CREATE TABLE Roles(\r\n" + 
					"	Role_ID int\r\n" + 
					"	,Roleof varchar(20)\r\n" + 
					"	,PRIMARY KEY(Role_ID)\r\n" + 
					");" +
					"CREATE TABLE MONEYRE(\r\n" + 
					"	MONEYBACK_ID SERIAL\r\n" + 
					"	,Moneyask money \r\n" + 
					"	,Timein  date not null default current_date-- set value default (current timestamp)\r\n" + 
					"	,TimeResolv date default current_date -- set value default (current timestamp)\r\n" + 
					"	,Desciption varchar(500)\r\n" + 
					"	,Asking int\r\n" + 
					"	,Resolving int\r\n" + 
					"	,Status_of int\r\n" + 
					"	,typeAsk int\r\n" + 
					"	,PRIMARY KEY (MONEYBACK_ID)\r\n" + 
					"	, FOREIGN KEY (ASKING) REFERENCES Gamer_User (Gamer_ID)\r\n" + 
					"	, FOREIGN KEY (RESOLVING) REFERENCES Gamer_User (Gamer_ID)\r\n" + 
					"	, FOREIGN KEY (Status_of) REFERENCES TYPEOF (Typeof_ID)\r\n" + 
					"	, FOREIGN KEY (typeAsk) REFERENCES STATUS (status_ID)\r\n" + 
					");" + 
					"INSERT INTO STATUS values(1, 'Pending');\r\n" + 
					"INSERT INTO STATUS VALUES(2, 'Accepted');\r\n" + 
					"INSERT INTO STATUS VALUES(3, 'Denied');\r\n" + 
					"\r\n" + 
					"INSERT INTO TYPEOF VALUES(1, 'TRAVEL');\r\n" + 
					"INSERT INTO TYPEOF VALUES(2, 'EQUIPMENT');\r\n" + 
					"INSERT INTO TYPEOF VALUES(3, 'MISC');\r\n" + 
					"insert into TYPEOF VALUES(4, 'FOOD');\r\n" + 
					"\r\n" + 
					"INSERT INTO ROLES VALUES(0, 'NOT SCENE YET');\r\n" + 
					"INSERT INTO ROLES VALUES(1, 'SCRIBE');\r\n" + 
					"INSERT INTO ROLES VALUES(2, 'BADASS');" +
					"INSERT INTO MONEYRE VALUES(1, 9388, default, NULL, 'testing purposes', 1, 3, 1, 2);\r\n" + 
					"INSERT INTO MONEYRE VALUES(2, 500, default, NULL, 'testing purposes', 2, 3, 2, 1);\r\n" + 
					"INSERT INTO MONEYRE VALUES(3, 1500, default, NULL, 'testing purposes', 2, 3, 2, 1);" +
					"Create view Table_data as\r\n" + 
					"Select moneyback_id, moneyask, timein, timeresolv, desciption, gu.gamer_usename, typeof_re, status, gu2.gamer_usename as resolver  \r\n" + 
					"From moneyre m INNER JOIN gamer_user gu on m.asking = gu.gamer_id \r\n" + 
					"inner join typeof t2 on m.status_of = t2.typeof_id \r\n" + 
					"inner join status s2 on m.typeask = s2.status_id \r\n" + 
					"inner join gamer_user gu2 on m.resolving = gu2.gamer_id ;\r\n" + 
					"";
			
			
			
		}catch(SQLException e) {
			e.printStackTrace();
			LogUtil.logger.warn("Exception creating a new customer", e);
		}
		
		
	}

	
		
	

}
