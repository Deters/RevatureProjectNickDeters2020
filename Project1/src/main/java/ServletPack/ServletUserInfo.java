package ServletPack;
import javax.servlet.http.HttpServlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import Logi4jPack.LogUtil;
public class ServletUserInfo extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		System.out.println("In UserInfo Servlet doGet");
	
		LogUtil.logger.info("User Logged Out");
		UserInfoRequestHelper.help(req,res);
		//res.sendRedirect("http://localhost:9001/Project1/ServletMain");
		//res.sendRedirect("https://www.google.com");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		System.out.println("In UserInfo Servlet Dopost");
		LogUtil.logger.info("User Logged Out");
		
		if(req.getSession(false) != null) {
			req.getSession(false);
			System.out.println("In the Servlet User and trying to set session to false");
		}
		
		
		res.sendRedirect("http://localhost:9001/Project1/resources/html/Index.html");
		
		//UserInfoRequestHelper.help(req,res);
		
		//the "new ObjectMapper" stuff is jackson databind at work
		
	}
	

}
