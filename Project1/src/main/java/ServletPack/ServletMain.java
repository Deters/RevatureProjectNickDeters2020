package ServletPack;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import Logi4jPack.LogUtil;
import Models.ReImbModel;

public class ServletMain extends HttpServlet{
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		LogUtil.logger.info("We are in the Sermain doGet");
		System.out.println("In ServlettMain , doGet");
		
		/*
		 * the default content type is text/html
		 * so the following line is redundant
		 */
		//res.setContentType("text/html");
		
		/*
		 * What is the PrintWriter object?
		 * 	I'ts an object used to DIRECTLY respond to the client
		 */
		//PrintWriter printer = res.getWriter();
		//printer.println("<html><body><h1>HELLO FROM MY SERVLET!!!!</h1></body></html>");
		
		
		/***
		 * @author Nicho
		 * This right here is when I first go to the website. I don't care who it is,
		 * I just want them to go to my login page
		 */
		
		
		
		
		req.getRequestDispatcher(RequestHelper.process(req, res));
		//res.sendRedirect("http://localhost:9001/Project1/resources/html/Index.html");
		
		//req.getRequestDispatcher(RequestHelper.process(req)).forward(req, res);
		
		
		
		
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		System.out.println("In ServletMain, doPost");	
		
		LogUtil.logger.info("We are in the Servlet main post");
		/***
		 * @author Nicho
		 * What is happening here is that I am at first with this Servlet getting a session
		 * This SESSION HAPPENS WHEN WE FIRST ENTER THE SITE, THAT'S IT
		 * I have faith that I NOW HAVE A SESSION WITH DATA INSIDE OF IT
		 */
		
		
		
		
		req.getRequestDispatcher(RequestHelper.process(req, res)).forward(req, res);
		
		
		//////////////Let's see how to SEND a JSON
		
//		ReImbModel obj = new ReImbModel();
//		obj.setReImbId(5);
//		obj.setReImbAmount(5787);
//		obj.setTimeStampSub(0000);
//		obj.setTimeStampResolve(0000);
//		obj.setDescip(null);
//		obj.setAuthReImb(2);
//		obj.setResoReimb(1);
//		obj.setReImbType(1);
//		obj.setReImbStat(1);
		
		//this line is general core java
		//NORMALLY, you would get this information from somewhere like your DB
	
		
		//servlet logic EXAMPLE TO GET GOOD WITH SERVLETS
//		res.setContentType("application/json");
//		
//		PrintWriter printer = res.getWriter();
//		
//		//the "new ObjectMapper" stuff is jackson databind at work
//		//printer.write(new ObjectMapper().writeValueAsString(obj));
//		printer.write("I have loged in");
	}
	
	///THIS WAS FOR TESTING SERVLET PURPOSES I WON'T USE THIS IN MY LOGGIN PAGE
//	@Override
//	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		
//		//////////////Let's see how to RECEIVE a JSON
//		
//		System.out.println("In ServletMain, doPut");
//		
//		resp.setContentType("application/json");
//		
//		//This is how we send a status code
//		//resp.setStatus(418);
//		//resp.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
//		resp.setStatus(HttpServletResponse.SC_CREATED);
//		
//		//let's ready a JSON from the user
//		ObjectMapper mapper = new ObjectMapper();
//		
//		ReImbModel objInternet = mapper.readValue(req.getInputStream(), ReImbModel.class);
//		
//				
//		
//		//let's send the user a status message to let them know everything went okay
//		System.out.println("It didn't break...THIS TIME");
//		System.out.println(objInternet);
//		
//		
//		
//	}
//
//	

}
