package ServletPack;

import javax.servlet.http.HttpServlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import Logi4jPack.LogUtil;
import Models.ReImbModel;

public class ServletNotMain extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		System.out.println("In NotMain doGet");
		LogUtil.logger.info("We are in Servlet Not Main Do get");
		
		
	
		ServletNotMainRequestHelper.process(req,res);
		//res.sendRedirect("http://localhost:9001/Project1/ServletMain");
		//res.sendRedirect("https://www.google.com");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		System.out.println("In NotMain Dopost");
		LogUtil.logger.info("We are in Servlet Not Main Do get");
		ServletNotMainRequestHelper.process(req,res);
		
		//the "new ObjectMapper" stuff is jackson databind at work
		
	}
	
	

}
