package ServletPack;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;

import ControllerPack.AdminCredentialController;
import ControllerPack.AdminLookController;
import ControllerPack.AdminSendController;
import ControllerPack.UserLookController;
import ControllerPack.UserSendController;
import Logi4jPack.LogUtil;

public class ServletNotMainRequestHelper {

	public static void process(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		System.out.println("In Not main Request helper");
		System.out.println(req.getRequestURI());
		LogUtil.logger.info("We got a URL Request in helper " + req.getRequestURL());
		
		switch (req.getRequestURI()) {
		case "/Project1/ServletNotMain/users":
			UserLookController.DisplayFinder(req, res);
			break;
		
		case "/Project1/ServletNotMain/send":
			UserSendController.SendJson(req, res);
			break;
			
		case "/Project1/ServletNotMain/adminsee":
			AdminLookController.Display(req, res);
			break;
			
		case "/Project1/ServletNotMain/admincredentials":
			AdminCredentialController.Display(req, res);
			break;
		
		case"/Project1/ServletNotMain/updateReq":
			AdminSendController.SendJ(req,res);
		
		
		}
		
		
		
	}

}
