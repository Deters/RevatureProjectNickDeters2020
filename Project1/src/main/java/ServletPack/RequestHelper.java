package ServletPack;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;

import ControllerPack.AdminController;
import ControllerPack.HomeController;
import ControllerPack.HomePageController;
import ControllerPack.LoginController;
import ControllerPack.ReImbController;
import Logi4jPack.LogUtil;

public class RequestHelper {
	
	public static String process(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		System.out.println("in Request Helper");
		System.out.println(req.getRequestURL());
		LogUtil.logger.info("We got a URL Request in helper " + req.getRequestURL());
		
		switch(req.getRequestURI()) {
		
		case "/Project1/ServletMain":
			System.out.println("case 0");
			return HomePageController.HomeIndex(req, res);
		
		
		case "/Project1/forwarding/login":
			System.out.println("case1");
			res.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
			return LoginController.login(req, res);
		
		case "/Project1/forwarding/home":
			System.out.println("case 2");
			res.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
			return HomeController.login(req, res);
			
		case "/Project1/forwarding/ReimbPage":
			System.out.println("case 3");
			res.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
			return ReImbController.make(req, res);
			
			
		case "/Project1/forwarding/scribeHome":
			System.out.println("case 4");
			res.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
			return AdminController.login(req,res);
		
		case "/Project1/forwarding/logout":
			System.out.println("case 5");
			 LoginController.logout(req, res);
			return null;
			default:
				System.out.println("bad Login");
				res.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
				return "/resources/html/BadLogin.html";
				
		}
		
		
		
		
	}

}
