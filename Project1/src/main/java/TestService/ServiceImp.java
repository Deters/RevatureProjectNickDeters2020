package TestService;

import java.util.ArrayList;
import java.util.List;

import DAOPack.ReimbDAO;
import DAOPack.ReimbDAOimpl;
import Logi4jPack.LogUtil;
import Models.ReImbModel;
import Models.ReUserModel;
import Models.SubmitModel;
import Models.UserView;
import Models.WhatKindModel;

public class ServiceImp implements ServiceLayer {

	private ReimbDAO DAOing = new ReimbDAOimpl();
	
	
	

	/*
	 * This was used to test that if I could actually insert into the database
	 * Not being used now
	 */
	@Override
	public void insertReimburseReq(ReImbModel obj) {
		LogUtil.logger.info("We are inserting a new request");
		DAOing.insertMethod(obj);
		
	}


	@Override
	public boolean ServiceImp( String username, String password) {
		LogUtil.logger.info("We are getting all info to check if the password and username are correct");
		List<ReUserModel> LoginList = DAOing.seeAllList();
		for(ReUserModel p: LoginList) {
			if(username.equals(p.getUseName()) && password.equals(p.getUsePass())){
				return true;
				
			}
		}
		
		return false;
		//DAOing.
		
	}


	@Override
	public List<UserView> getUserView(String username, String password) {
		List<UserView> theUseView = DAOing.getViewList();
		List<UserView> PendingView = DAOing.getPendingList();
		List<UserView> sortList = new ArrayList<>();
		LogUtil.logger.info("We are getting the users");
		for(UserView p: theUseView ) {
			if(p.getUseName().equals(username)) {
				//System.out.println(p.getUseName() + " this is in the ServiceImp");
				sortList.add(p);
			}
		}
		
		for(UserView p: PendingView ) {
			if(p.getUseName().equals(username)) {
				//System.out.println(p.getUseName() + " this is in the ServiceImp");
				sortList.add(p);
			}
		}
		
		
		return sortList;
	}


	@Override
	public boolean CheckAdmin(String username, String password) {
		List<WhatKindModel> CheckView = DAOing.getWhatKindList();
		LogUtil.logger.info("We are checking to see if the login is an admin");
		for(WhatKindModel p: CheckView) {
			if(p.getUseName().equals(username)){
				if(p.getRoleOf().equals("SCRIBE")) {
					return true;
				}
				
				
				
			}
		}
		
		
		return false;
	}


	@Override
	public void GetGamer(ReUserModel user, String loginName) {
		LogUtil.logger.info("We are getting a full gamer object from the database");
		DAOing.getGamerobj(user, loginName);
		
	}


	@Override
	public void insertReq(ReImbModel request) {
		DAOing.insertRequest(request);
		
	}


	@Override
	public List<UserView> getAdminView() {
		List<UserView> theDiffList = DAOing.getViewReciepts();
		List<UserView> PendingView = DAOing.getPendingList();
		for(UserView p: PendingView ) {
			theDiffList.add(p);
		}
		
		LogUtil.logger.info("We got the 2 lists from database and put them together");
		return theDiffList;
	}


	@Override
	public void insertstatusChange(ReImbModel resolving) {
		LogUtil.logger.info("We are changing a state of an Employee");
		DAOing.InsertStatChange(resolving);
		
	}


	


	
	
	
	
//	private TestDAO TESTDAO = new TestDAOImp();

//	@Override
//	public void insertServTest(TestingModel obj) {
//		//This is where I would do business Logic
//		TESTDAO.insertTest(obj);	
//	}

}
