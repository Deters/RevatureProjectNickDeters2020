package TestService;

import java.util.List;

import Models.ReImbModel;
import Models.ReUserModel;
import Models.SubmitModel;
import Models.UserView;

public interface ServiceLayer {
	
	
	

	boolean ServiceImp( String username, String password);
	
	void insertReimburseReq(ReImbModel obj);

	public List<UserView> getUserView(String username, String password);

	boolean CheckAdmin(String username, String password);

	void GetGamer(ReUserModel user, String loginName);

	void insertReq(ReImbModel request);

	List<UserView> getAdminView();

	void insertstatusChange(ReImbModel resolving);

	
	

}
