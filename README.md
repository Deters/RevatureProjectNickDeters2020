Project 1 Gitlab for Revature Portfolio


# PROJECT NAME
Revature Reimbursement 
## Project Description

This project is a simple reimbursement system linked with a database. This project utilizes a tomcat servlet to deploy on a local host machine
to give a webpage that will be the users view to interact with. From here there are predefined administrators who process requests and the 
users who will make a request. 

## Technologies Used

* Maven Project set up with dependencies
* JDBC Postgesql database
* ARDS database
* servlet tomcat through local host 
* HTML, CSS, JS Front end
* Ajax request helper servlet to send and recieve JSON packages
* Logj4 Logger set up
* JUnit Testing
* DAO OOP Design Pattern through Java 8

## Features

List of features ready and TODOs for future development
* Custom views within DB with joined persistant tables
* Custom input checks in front end Java to restrict unusable JSON in the back end
* Login checking with verification through DB that will bring users and administrators to different 

To-do list:
* Making users from the administrators page
* password encryption stored in the database with keys 
* h2 Testing mock database

## Getting Started
   
to start this project use git clone https://gitlab.com/Deters/RevatureProjectNickDeters2020
From here use you may use your prefered IDE however this project was set up with sts and will work well with 
importing the project. https://spring.io/tools#suite-three
To import this Project go to your IDE and select file->import->ExistingMavenproject
From here find the clone project called "Project1" select the pom file and import this project

from here we should set up the tomcat servlet it will deploy on 
first we must download it (I recommend winwar it's free) from https://tomcat.apache.org/download-90.cgi
download the core file and extract it to an easy to find place 
from here we must put it onto the workspace through these easy steps in our workspace
window -> preferences
server -> runtime environments
add -> tomcat ->version 9       in apache
find tomcat -> hit browse get the folder that has the bin library and logs
hit finish -> apply and close
From here make sure to verify it is set up by checking the server tab. We can change which port by double clicking tomcat and 
checking the server ports (picture)

From here yes it will still have an error in the project so we want to deploy all of its neccessary wiring. 
Follow these steps if you see the project still is not able to compile 

project -> properties -> targeted runtimes ->apache tomcat -> apply and close

To add the project to you tomcat server. You must do this so Tomcat will deploy this project 
servers right click servers -> Add and Remove -> move Project to right side with add and remove. 

After the Project is set its now time to set up the Connection to the actual database. 
In my Connection.java and ReimbDAOImpl.java url, username and password.
These are the connections that you will be using for a database, for security reason they are system variables. 
Change them to your DB credentials using your prefered method either system variables or hardcoded credentials


After this start the Tomcat server by going to servers and clicking the start button, from there it will deploy to local host port 
of you choosing. After that it should be correctly set up and deployed properly


## Usage

When starting up the web page go to http://localhost:(your prot)/Project1/resources/html/Index.html for the Login page
After login to the respective pages either an employee or administrators
In the employee requests you can make a request with the three fields given. The first one has to be a number for the amount. 
don't worry about trying to input it with any special form the database will convert it to 2 decimal places and round to them 
if you input more than 2 numbers after the number. Also then you can input a description with no parameters because it will save as a 
string in the database. After the 4 input fields are food, misc, travel and equipment. If this field is not set it will go default
to misc.

In the administrators page there is a button to send the request to see all reciepts it disables after we click it but we pull
all the reciepts from the database with their ID and information into 3 lists, pending, accepted and denied. We can change the status
on all the pending to either denied or accepted, if the field is not set it defaults into denied. There is also the field for the ID
of the, this has front end handling that will only let you change an ID for something in the pending list, as well as a valid ID as well. 


